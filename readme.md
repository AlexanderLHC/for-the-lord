# "For the Lored" vs "Lored"

Idea or scratchpad for an addon that is supposed to present users with additional lore.
The conditions to show lore:
	* In zone
	* Have quest
	* Is targeted


Feature list:
1. Have settings in "Addons menu" with togleable settings
1. There must be a hierarchy of "conditions" that checks whether to show.
*. There should be "ignore feature" to cancel out certain lore
*. Tracker of how much lore you've seen/read
*. Make how-to get lore added 
*. Conditions to load:
	- Have conditions folder containing subfolder of each condition (ideas)
		- Location
		- Target
		- Have quest
	- Each of these files will be named by id or their condition and contains the lore part
		- make each condition able to call content from another condition. So if in zone is true then load if X is target.
*. When condition is met show icon if you click icon you open "Lore interface" that shows list of current active conditions.

